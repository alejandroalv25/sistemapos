package com.gead.sistemapos.controller.request;

public class Response {

	public Response(String codigo, String message) {
		super();
		this.codigo = codigo;
		this.message = message;
	}

	private String codigo;
	private String message;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
