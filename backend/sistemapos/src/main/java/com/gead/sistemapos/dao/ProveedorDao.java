package com.gead.sistemapos.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.gead.sistemapos.domain.entity.Proveedor;


@Transactional
public interface ProveedorDao extends JpaRepository<Proveedor, Integer> {
	
	Page<Proveedor> findAll(Pageable pageable);
	
}
