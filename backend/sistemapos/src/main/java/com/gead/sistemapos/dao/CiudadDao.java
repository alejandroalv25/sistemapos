package com.gead.sistemapos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gead.sistemapos.domain.entity.Ciudad;
import com.gead.sistemapos.domain.entity.Proveedor;

public interface CiudadDao extends JpaRepository<Ciudad, Integer>{
	
	List<Ciudad> findByIdDepartamento(int departamento);

}
