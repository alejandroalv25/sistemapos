package com.gead.sistemapos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gead.sistemapos.domain.entity.Usuario;

public interface UsuarioDao extends JpaRepository<Usuario, Integer> {
	
	@Query("SELECT p FROM Usuario p WHERE LOWER(p.correo) = LOWER(:correo) and LOWER(p.password) = LOWER(:password)")
    public List<Usuario> findByUsuarioAndContrasena(@Param("correo") String correo, @Param("password") String password);

}
