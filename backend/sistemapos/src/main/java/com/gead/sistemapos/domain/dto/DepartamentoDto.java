package com.gead.sistemapos.domain.dto;

import java.util.List;

public class DepartamentoDto {
		
    private Integer id;
    private String nombre;
    //private List<Ciudad> ciudadList;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/*
	public List<Ciudad> getCiudadList() {
		return ciudadList;
	}
	public void setCiudadList(List<Ciudad> ciudadList) {
		this.ciudadList = ciudadList;
	}*/
    
    

}
