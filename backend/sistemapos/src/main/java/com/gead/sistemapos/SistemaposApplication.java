package com.gead.sistemapos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaposApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaposApplication.class, args);
	}

}
