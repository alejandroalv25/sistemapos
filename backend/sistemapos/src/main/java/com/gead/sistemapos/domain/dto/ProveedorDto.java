package com.gead.sistemapos.domain.dto;

public class ProveedorDto {

	private Integer id;
	private String idUsuario;
	private String idSede;
	private String nit;
	private String nombres;
	private String telefono;
	private String direccion;
	//Nuevos campos
	private String email;
	private String tipodocumento;
	private String documento;
	private String departamento;
	private String ciudad;
	
	public ProveedorDto() {}

	public ProveedorDto(Integer id, String idUsuario, String idSede, String nit, String nombres, String telefono,
			String direccion, String email, String tipodocumento, String documento, String departamento,
			String ciudad) {
		super();
		this.id = id;
		this.idUsuario = idUsuario;
		this.idSede = idSede;
		this.nit = nit;
		this.nombres = nombres;
		this.telefono = telefono;
		this.direccion = direccion;
		this.email = email;
		this.tipodocumento = tipodocumento;
		this.documento = documento;
		this.departamento = departamento;
		this.ciudad = ciudad;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getIdSede() {
		return idSede;
	}

	public void setIdSede(String idSede) {
		this.idSede = idSede;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipodocumento() {
		return tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Override
	public String toString() {
		return "Proveedor [id=" + id + ", idUsuario=" + idUsuario + ", idSede=" + idSede + ", nit=" + nit + ", nombres="
				+ nombres + ", telefono=" + telefono + ", direccion=" + direccion + "]";
	}

}
