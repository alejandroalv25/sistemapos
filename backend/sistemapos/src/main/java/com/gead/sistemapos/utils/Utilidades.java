package com.gead.sistemapos.utils;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.gead.sistemapos.domain.dto.ProveedorDto;
import com.gead.sistemapos.domain.entity.Proveedor;

public class Utilidades {
	
	public static ProveedorDto convertToProveedorDto(Proveedor proveedor) {
		ProveedorDto proveedorDto = new ProveedorDto();
		try {
			BeanUtils.copyProperties(proveedorDto, proveedor);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return proveedorDto;
		
	}
	
	public static Proveedor convertToProveedorEntity(ProveedorDto proveedorDto) {
		Proveedor proveedor = new Proveedor();
		try {
			BeanUtils.copyProperties(proveedor,  proveedorDto);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return proveedor;
		
	}

}
