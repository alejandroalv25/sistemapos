package com.gead.sistemapos.controller;

import org.springframework.web.bind.annotation.RestController;

import com.gead.sistemapos.controller.request.Response;
import com.gead.sistemapos.dao.CiudadDao;
import com.gead.sistemapos.dao.DepartamentoDao;
import com.gead.sistemapos.dao.ProveedorDao;
import com.gead.sistemapos.dao.UsuarioDao;
import com.gead.sistemapos.domain.dto.CiudadDto;
import com.gead.sistemapos.domain.dto.DepartamentoDto;
import com.gead.sistemapos.domain.dto.ProveedorDto;
import com.gead.sistemapos.domain.entity.Ciudad;
import com.gead.sistemapos.domain.entity.Departamento;
import com.gead.sistemapos.domain.entity.Proveedor;
import com.gead.sistemapos.domain.entity.Usuario;
import com.gead.sistemapos.utils.Utilidades;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.catalina.mapper.Mapper;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class ProveedorController {

	@Autowired
	ProveedorDao proveedorDao;

	@Autowired
	CiudadDao ciudadDao;

	@Autowired
	UsuarioDao usuarioDao;

	@Autowired
	DepartamentoDao departamentoDao;

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@GetMapping("/lisadoDepartamento")
	List<DepartamentoDto> lisadoDepartamento() {

		List<DepartamentoDto> listadoCiudades = new ArrayList<DepartamentoDto>();

		for (Departamento item : departamentoDao.findAll()) {
			DepartamentoDto ciudadDto = new DepartamentoDto();
			try {
				BeanUtils.copyProperties(ciudadDto, item);
			} catch (IllegalAccessException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			listadoCiudades.add(ciudadDto);
		}

		return listadoCiudades;
	}

	@GetMapping("/lisadoCiudad/{idDepartamento}")
	List<CiudadDto> listadoCiudad(@PathVariable Integer idDepartamento) {

		List<CiudadDto> listadoCiudades = new ArrayList<CiudadDto>();
		for (Ciudad item : ciudadDao.findByIdDepartamento(idDepartamento)) {
			CiudadDto ciudadDto = new CiudadDto();
			try {
				BeanUtils.copyProperties(ciudadDto, item);
			} catch (IllegalAccessException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			listadoCiudades.add(ciudadDto);
		}

		return listadoCiudades;
	}

	@GetMapping("/listadoProveedores")
	List<ProveedorDto> listadoProveedores() {

		List<ProveedorDto> listadoProveedor = new ArrayList<ProveedorDto>();
		for (Proveedor item : proveedorDao.findAll()) {
			ProveedorDto proveedorDto = new ProveedorDto();
			try {
				BeanUtils.copyProperties(proveedorDto, item);
			} catch (IllegalAccessException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			listadoProveedor.add(proveedorDto);
		}

		return listadoProveedor;
	}

	@GetMapping("/listadoProveedores2/{page}/{filtro}")
	List<ProveedorDto> listadoProveedores2(@PathVariable Integer page, @PathVariable String filtro) {

		System.out.println(filtro);
		if (!filtro.equals("null") && filtro != null && filtro.trim().length() != 0) {

			List<ProveedorDto> listadoProveedor = new ArrayList<ProveedorDto>();
			List<Proveedor> pageTuts = proveedorDao.findAll();

			List<Proveedor> lista = pageTuts.stream().filter(p -> p.getNombres().startsWith(filtro))
					.collect(Collectors.toList());

			for (Proveedor proveedor : lista) {

				ProveedorDto proveedorDto = new ProveedorDto();
				try {
					BeanUtils.copyProperties(proveedorDto, proveedor);
				} catch (IllegalAccessException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				listadoProveedor.add(proveedorDto);
			}
			return listadoProveedor;
		} else {
			List<ProveedorDto> listadoProveedor = new ArrayList<ProveedorDto>();
			Pageable paging = PageRequest.of(page, 9);
			Page<Proveedor> pageTuts = proveedorDao.findAll(paging);

			for (Proveedor item : pageTuts.getContent()) {

				ProveedorDto proveedorDto = new ProveedorDto();
				try {
					BeanUtils.copyProperties(proveedorDto, item);
				} catch (IllegalAccessException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				listadoProveedor.add(proveedorDto);
			}

			return listadoProveedor;
		}

	}

	@PostMapping("/guardarProveedor")
	Response guardarProveedor(@RequestBody ProveedorDto proveedorDto) {
		System.out.println(proveedorDto.toString());
		proveedorDao.save(Utilidades.convertToProveedorEntity(proveedorDto));
		return respuestaCorrecta();
	}

	@PostMapping("/actualizarProveedor")
	Response actualizarProveedor(@RequestBody ProveedorDto proveedor) {
		proveedorDao.save(Utilidades.convertToProveedorEntity(proveedor));
		return respuestaCorrecta();
	}

	@GetMapping("/eliminarProveedor/{id}")
	Response eliminarProveedor(@PathVariable Integer id) {
		System.out.println("inicio eliminar");
		System.out.println(id);
		Optional<Proveedor> proveedor = proveedorDao.findById(id);
		proveedorDao.delete(proveedor.get());
		return respuestaCorrecta();
	}

	@GetMapping("/login/{correo}/{contrasena}")
	Response login(@PathVariable String correo, @PathVariable String contrasena) {
		List<Usuario> proveedor = usuarioDao.findByUsuarioAndContrasena(correo, contrasena);

		if (proveedor.size() > 0) {
			return respuestaCorrecta();
		} else {
			return respuestaSinPermisos();
		}

	}

	public Response respuestaCorrecta() {
		return new Response("200", "ok");
	}

	public Response respuestaSinPermisos() {
		return new Response("403", "error");
	}

}