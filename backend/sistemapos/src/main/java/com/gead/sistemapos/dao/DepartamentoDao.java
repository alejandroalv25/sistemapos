package com.gead.sistemapos.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gead.sistemapos.domain.entity.Departamento;
import com.gead.sistemapos.domain.entity.Proveedor;

public interface DepartamentoDao extends JpaRepository<Departamento, Integer>{

}
