import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sistemapos/presentacion/home_page.dart';
import 'package:sistemapos/presentacion/login_home.dart';
import 'package:sistemapos/presentacion/proveedor_creacion_page.dart';
import 'package:sistemapos/utilidades/PreferenciasUsuario.dart';

import 'presentacion/proveedores_page.dart';
import 'presentacion/provider/ManejadorEstado.dart';

void main() async{

  final prefs = new PreferenciasUsuario();
  WidgetsFlutterBinding();
  await prefs.initPrefs();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
            create: (BuildContext context) => ManejadorEstado())
      ],
      child: MaterialApp(
        initialRoute: 'login',
        routes: {
          'login': (BuildContext context) => LoginPage(),
          'home': (BuildContext context) => HomePage(),
          'proveedores': (BuildContext context) => ProveedoresPage(),
          'proveedorCreacion': (BuildContext context) =>
              ProveedorCreacionPagina(),
        },
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
      ),
    );
  }
}
