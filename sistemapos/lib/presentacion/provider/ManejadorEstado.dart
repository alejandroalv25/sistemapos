import 'package:flutter/cupertino.dart';
import 'package:sistemapos/dominio/entities/user_entity.dart';

class ManejadorEstado extends ChangeNotifier {
  
  int _opcionSeleccionada;
  List<User> _listadoProveedores = new List<User>();
  int _countPages = 0;

  int get countPages{
    return this._countPages;
  }

  set countPages(int i){
    this._countPages = i;
    notifyListeners();
  }

  List<User> get listadoProveedores {
    if(this._listadoProveedores.length > 0){
      return this._listadoProveedores;
    }else{
      return [];
    }
    
  }

  set listadoProveedores (List<User> userList){
    this._listadoProveedores = userList;
    notifyListeners();
  }


  int get opcionSeleccionada {
    return this._opcionSeleccionada;
  }

  set opcionSeleccionada(int i) {
    this._opcionSeleccionada = i;
    notifyListeners();
  }
}
