import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sistemapos/data/Servicios/Services.dart';
import 'package:sistemapos/dominio/entities/user_entity.dart';

import 'provider/ManejadorEstado.dart';

class ProveedoresPage extends StatefulWidget {
  final String title = "Mis proveedores";

  @override
  _ProveedoresPageState createState() => _ProveedoresPageState();
}

class _ProveedoresPageState extends State<ProveedoresPage> {
  List<User> users;
  List<User> selectedUsers;
  bool sort;
  String filtroBusqueda = '';
  Services servicio = new Services();

  List<User> listadoProveedores = [];

  ScrollController escrollController = new ScrollController();

  @override
  initState() {
    sort = false;
    selectedUsers = [];
    users = User.getUsers();
    super.initState();
    final provider = Provider.of<ManejadorEstado>(context, listen: false);
    provider.countPages = 0;
    servicio.listadoProveedores2(provider.countPages, 'null').then((value) {
      provider.listadoProveedores = value;
      setState(() {});
    });

    escrollController.addListener(() {
      if (filtroBusqueda == '' || filtroBusqueda.trimRight() == 'null') {
        if (escrollController.position.pixels ==
            escrollController.position.maxScrollExtent) {
          provider.countPages = provider.countPages + 1;
          servicio
              .listadoProveedores2(provider.countPages, 'null')
              .then((value) {
            provider.listadoProveedores.addAll(value);
            setState(() {});
          });
          setState(() {});
        }
      }
    });
  }

  onSortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        users.sort((a, b) => a.nombres.compareTo(b.nombres));
      } else {
        users.sort((a, b) => b.nombres.compareTo(a.nombres));
      }
    }
  }

  onSelectedRow(bool selected, User user) async {
    setState(() {
      if (selected) {
        selectedUsers.add(user);
      } else {
        selectedUsers.remove(user);
      }
    });
  }

  deleteSelected() async {
    setState(() {
      if (selectedUsers.isNotEmpty) {
        List<User> temp = [];
        temp.addAll(selectedUsers);
        for (User user in temp) {
          users.remove(user);
          selectedUsers.remove(user);
        }
      }
    });
  }

  SingleChildScrollView dataBody() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        sortAscending: sort,
        sortColumnIndex: 0,
        columns: [
          DataColumn(
              label: Text("Nombres"),
              numeric: false,
              tooltip: "Columna Nombres",
              onSort: (columnIndex, ascending) {
                setState(() {
                  sort = !sort;
                });
                onSortColum(columnIndex, ascending);
              }),
          DataColumn(
            label: Text("Apellidos"),
            numeric: false,
            tooltip: "Columna apellidos",
          ),
          DataColumn(
            label: Text("Dirección"),
            numeric: false,
            tooltip: "Columna dirección",
          ),
          DataColumn(
            label: Text("Telefono"),
            numeric: false,
            tooltip: "Columna telefono",
          ),
        ],
        rows: users
            .map(
              (user) => DataRow(
                  selected: selectedUsers.contains(user),
                  onSelectChanged: (b) {
                    onSelectedRow(b, user);
                  },
                  cells: [
                    DataCell(
                      Text(user.nombres),
                      onTap: () {
                      },
                    ),
                    DataCell(
                      Text(user.direccion),
                    ),
                    DataCell(
                      Text(user.direccion),
                    ),
                    DataCell(
                      Text(user.telefono),
                    ),
                  ]),
            )
            .toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        verticalDirection: VerticalDirection.down,
        children: <Widget>[
          barraBusqueda(),
          Expanded(
            //child: dataBody(),
            child: _myListView2(context),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FloatingActionButton(
                  heroTag: "btn1",
                  onPressed: () async {
                    Navigator.pushNamed(context, 'proveedorCreacion',
                        arguments: new User());

                    // Add your onPressed code here!
                  },
                  child: Icon(Icons.add),
                  backgroundColor: Colors.green,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _myListView2(BuildContext context1) {
    final provider = Provider.of<ManejadorEstado>(context1);

    return RefreshIndicator(
      onRefresh: cargarConsultaPaginada,
      child: ListView.builder(
        controller: escrollController,
        itemCount: provider.listadoProveedores.length,
        itemBuilder: (context, index) {
          final item = provider.listadoProveedores[index];
          return Card(
            child: Dismissible(
              background: Container(color: Colors.green),
              key: Key(item.id.toString()),
              onDismissed: (direction) {
                setState(() {
                  servicio
                      .eliminarProveedor(provider.listadoProveedores[index].id)
                      .then((value) {});
                  provider.listadoProveedores.removeAt(index);
                });

                Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text("Se ha borrado correctamente.")));
              },
              child: ListTile(
                title: Text(item.nombres),
                subtitle: Text('     ' +
                    item.tipodocumento +
                    ': ' +
                    item.nit +
                    " " +
                    "Telefono: " +
                    item.telefono),
                onTap: () {
                  setState(() {
                    Navigator.pushNamed(context, 'proveedorCreacion',
                        arguments: item);
                  });
                },
                onLongPress: () {
                  setState(() {});
                },
              ),
            ),
          );
        },
      ),
    );
  }

  carcarInformacionProveedores() async {
    final provider = Provider.of<ManejadorEstado>(context);
    servicio.listadoProveedores2(provider.countPages, 'null').then((value) {
      provider.listadoProveedores = value;
      setState(() {});
    });
  }

  Widget barraBusqueda() {
    final provider = Provider.of<ManejadorEstado>(context, listen: false);
    return Container(
      color: Colors.white,
      margin: EdgeInsets.all(30),
      child: TextField(
          onChanged: (value) {
            provider.countPages = 0;
            filtroBusqueda = value;
            if (value == '') {
              filtroBusqueda = 'null';
            }
            servicio
                .listadoProveedores2(provider.countPages, filtroBusqueda)
                .then((value) {
              provider.listadoProveedores = value;
              setState(() {});
            });
          },
          decoration: InputDecoration(
            labelText: 'Buscar aqui',
            prefixIcon: Icon(Icons.search),
            contentPadding: EdgeInsets.all(5),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(
                width: 8,
                style: BorderStyle.none,
              ),
            ),
          )),
    );
  }

  Future<Null> cargarConsultaPaginada() async {
    final duration = new Duration(seconds: 2);
    return Future.delayed(duration);
  }
}
