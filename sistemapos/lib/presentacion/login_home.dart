import 'package:flutter/material.dart';
import 'package:sistemapos/data/Servicios/Services.dart';
import 'package:sistemapos/utilidades/PreferenciasUsuario.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _correo = '';
  String _contrasena = '';

  TextEditingController _correoEditingController = new TextEditingController();
  TextEditingController _contrasenaEditingController =
      new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    final _prefs = PreferenciasUsuario();

    _correo = _prefs.correo;
    _correoEditingController.text = _prefs.correo;

    _contrasena = _prefs.contrasena;
    _contrasenaEditingController.text = _prefs.contrasena;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [_fondoVerde(context), _login(context)]),
    );
  }

  Widget _login(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Column(children: [
      SafeArea(child: Container(height: 250)),
      Container(
        width: size.width * 0.80,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5)),
        child: Padding(
          padding: EdgeInsets.all(15),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Text("Ingreso"),
                SizedBox(height: 10),
                _email(),
                SizedBox(height: 10),
                _password(),
                SizedBox(height: 10),
                _raiseButton(context),
                SizedBox(height: 10),
                _withoutAccount(),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      )
    ]));
  }

  Widget _fondoVerde(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final cabecera = Container(
        height: size.height * 0.4, width: double.infinity, color: Colors.green);
    final titulo = Container(
      margin: EdgeInsets.only(top: 90),
      child: Center(
        child: Column(
          children: [
            Icon(
              Icons.account_circle,
              size: 100,
              color: Colors.white,
            ),
            SizedBox(height: 10),
            Text(
              'Control inventario',
              style: TextStyle(fontSize: 20, color: Colors.white),
            )
          ],
        ),
      ),
    );

    return Stack(children: [cabecera, titulo]);
  }

  Widget _email() {
    return TextFormField(
      controller: _correoEditingController,
      keyboardType: TextInputType.emailAddress,
      validator: (text) {
        if (text == null || text.isEmpty || text.trim().length == 0) {
          return 'Campo correo esta vacio.';
        }

        if (!text.contains('@')) {
          return 'Ingrese un correo valido';
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: 'Correo',
          icon: Icon(Icons.alternate_email, color: Colors.green)),
    );
  }

  Widget _password() {
    return TextFormField(
      obscureText: true,
      controller: _contrasenaEditingController,
      keyboardType: TextInputType.visiblePassword,
      validator: (text) {
        if (text == null || text.isEmpty || text.trim().length == 0) {
          return 'Campo contraseña esta vacio.';
        }

        return null;
      },
      decoration: InputDecoration(
          labelText: 'Cotranseña', icon: Icon(Icons.lock, color: Colors.green)),
    );
  }

  Widget _raiseButton(BuildContext context) {
    return RaisedButton(
      onPressed: () async {
        _correo = _correoEditingController.text;
        _contrasena = _contrasenaEditingController.text;

        final _prefs = PreferenciasUsuario();

        _prefs.correo = _correo;
        _prefs.contrasena = _contrasena;

        if (_formKey.currentState.validate()) {
          Services service = new Services();
          bool resultado = await service.login(_correo, _contrasena);
          if (resultado) {
            Navigator.pushNamed(context, 'home');
          } else {
            _alertaLogueo(context, 'Las credenciales no son validas.');
          }
        }
      },
      color: Colors.green,
      child: Text('Ingresar', style: TextStyle(color: Colors.white)),
    );
  }

  Widget _withoutAccount() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('No se ha registrado? '),
        InkWell(
            child: Text(
              'Registrate',
              style: TextStyle(color: Colors.green),
            ),
            onTap: () {})
      ],
    );
  }

  Widget _facebookButton() {
    return MaterialButton(
      onPressed: () {},
      color: Colors.blue,
      textColor: Colors.white,
      child: Icon(
        Icons.camera_alt,
        size: 24,
      ),
      padding: EdgeInsets.all(16),
      shape: CircleBorder(),
    );
  }

  Widget _anotherLogin() {
    return Row(
      children: [_facebookButton()],
    );
  }

  void _alertaLogueo(BuildContext context, String texto) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alerta'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(texto),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Aceptar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
