import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sistemapos/data/Servicios/Services.dart';
import 'package:sistemapos/dominio/entities/ciudad_entity.dart';
import 'package:sistemapos/dominio/entities/departamento_entity.dart';
import 'package:sistemapos/dominio/entities/user_entity.dart';

import 'provider/ManejadorEstado.dart';

class ProveedorCreacionPagina extends StatefulWidget {
  @override
  _ProveedorCreacionPaginaState createState() =>
      _ProveedorCreacionPaginaState();
}

class _ProveedorCreacionPaginaState extends State<ProveedorCreacionPagina> {
  String _nombre = '';
  String _telefono = '';
  String _direccion = '';
  String _tipoDocumento = 'CC';
  String _departamento = '0';
  String _ciudad = '0';
  String _email = '';
  String _documento = '';
  User proveedor = new User();

  TextEditingController nombreController = new TextEditingController();

  TextEditingController telefonoController = new TextEditingController();

  TextEditingController direccionController = new TextEditingController();

  TextEditingController emailController = new TextEditingController();

  TextEditingController documentoController = new TextEditingController();

  Services service = new Services();

  final scaffoldKey = GlobalKey<ScaffoldState>();

  List<Departamento> listadoDepartamento = [];
  List<Ciudad> listadoCiudad = [];

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      setState(() {
        User args = ModalRoute.of(context).settings.arguments;
        if (args != null && args.id != null) {
          nombreController.text = args.nombres;
          telefonoController.text = args.telefono;
          direccionController.text = args.direccion;
          emailController.text = args.email;
          documentoController.text = args.documento;
          _departamento = args.departamento;
          _ciudad = args.ciudad;
          _tipoDocumento = args.tipodocumento;
          proveedor = args;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Creacion de proveedores'),
      ),
      body: formularioRegistroProveedor(context),
    );
  }

  Widget formularioRegistroProveedor(BuildContext context1) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(5)),
      child: Padding(
        padding: EdgeInsets.all(15),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                _tipoDocumentoComponent(),
                SizedBox(height: 10),
                _camposDocumento(),
                SizedBox(height: 10),
                _camposNombres(),
                SizedBox(height: 10),
                _camposTelefono(),
                SizedBox(height: 10),
                _camposEmail(),
                SizedBox(height: 10),
                _camposDireccion(),
                SizedBox(height: 10),
                _dropdownListaDepartamentos(),
                SizedBox(height: 10),
                _dropdownListaCiudades(),
                SizedBox(height: 10),
                _botonCrearProveedor(context1),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _camposNombres() {
    return TextFormField(
      controller: nombreController,
      keyboardType: TextInputType.text,
      validator: (text) {
        if (text == null || text.isEmpty || text.trimRight().length == 0) {
          return 'Campo nombres esta vacio.';
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: 'Nombres',
      ),
    );
  }

  Widget _camposTelefono() {
    return TextFormField(
      controller: telefonoController,
      keyboardType: TextInputType.text,
      validator: (text) {
        if (text == null || text.isEmpty || text.trimRight().length == 0) {
          return 'Campo telefono esta vacio.';
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: 'Telefono',
      ),
    );
  }

  Widget _camposDireccion() {
    return TextFormField(
      controller: direccionController,
      keyboardType: TextInputType.text,
      validator: (text) {
        if (text == null || text.isEmpty || text.trimRight().length == 0) {
          return 'Campo direccion esta vacio.';
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: 'Dirección',
      ),
    );
  }

  Widget _camposEmail() {
    return TextFormField(
      controller: emailController,
      keyboardType: TextInputType.text,
      validator: (text) {
        if (text == null || text.isEmpty || text.trimRight().length == 0) {
          return 'Campo email esta vacio.';
        }
        if (!text.contains('@')) {
          return 'Ingrese un correo valido';
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: 'Email',
      ),
    );
  }

  Widget _camposDocumento() {
    return TextFormField(
      controller: documentoController,
      keyboardType: TextInputType.text,
      validator: (text) {
        if (text == null || text.isEmpty || text.trimRight().length == 0) {
          return 'Campo documento esta vacio.';
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: '# Documento',
      ),
    );
  }

  Widget _tipoDocumentoComponent() {
    return DropdownButtonFormField<String>(
      isExpanded: true,
      value: _tipoDocumento,
      onChanged: (String newValue) {
        setState(() {
          _tipoDocumento = newValue;
        });
      },
      items: <String>['CC', 'TI', 'Registro civil', 'Pasaporte']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  Widget _dropdownListaDepartamentos() {
    Services servicio = new Services();
    return FutureBuilder(
      future: servicio.litadoDepartamento(),
      builder:
          (BuildContext context, AsyncSnapshot<List<Departamento>> snapshot) {
        if (snapshot.hasData) {
          listadoDepartamento = snapshot.data;

          return Row(
            children: <Widget>[
              Icon(Icons.chevron_right, color: Colors.tealAccent[700]),
              SizedBox(width: 30.0),
              Expanded(
                child: DropdownButtonFormField(
                  value: _departamento,
                  items: getOpcionesListaDepartamento(),
                  onChanged: (String newValue) {
                    setState(() {
                      _departamento = newValue;
                      _ciudad = '0';
                      listadoCiudad = [];
                    });
                  },
                  onSaved: (value) {},
                ),
              )
            ],
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  List<DropdownMenuItem<String>> getOpcionesListaDepartamento() {
    List<DropdownMenuItem<String>> lista = new List();

    lista.add(DropdownMenuItem(
      child: Text("Departamentos"),
      value: '0',
    ));

    listadoDepartamento.forEach((tipo) {
      lista.add(DropdownMenuItem(
        child: Text(tipo.nombre),
        value: tipo.id.toString(),
      ));
    });

    return lista;
  }

  Widget _dropdownListaCiudades() {
    Services servicio = new Services();
    return FutureBuilder(
      future: servicio.litadoCiudad(_departamento ?? '0'),
      builder: (BuildContext context, AsyncSnapshot<List<Ciudad>> snapshot) {
        if (snapshot.hasData) {
          listadoCiudad = snapshot.data;

          return Row(
            children: <Widget>[
              Icon(Icons.chevron_right, color: Colors.tealAccent[700]),
              SizedBox(width: 30.0),
              Expanded(
                child: DropdownButtonFormField(
                  value: _ciudad,
                  items: getOpcionesListaCiudades(),
                  onChanged: (String newValue) {
                    setState(() {
                      _ciudad = newValue;
                    });
                  },
                  onSaved: (value) {},
                ),
              )
            ],
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  List<DropdownMenuItem<String>> getOpcionesListaCiudades() {
    List<DropdownMenuItem<String>> lista = new List();

    lista.add(DropdownMenuItem(
      child: Text("Ciudades"),
      value: '0',
    ));

    listadoCiudad.forEach((tipo) {
      lista.add(DropdownMenuItem(
        child: Text(tipo.nombre),
        value: tipo.id.toString(),
      ));
    });

    return lista;
  }

  Future mostrarSnackbar(String mensaje) {
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(seconds: 6),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);

    return new Future.delayed(const Duration(seconds: 1), () => "1");
  }

  Widget _botonCrearProveedor(BuildContext context1) {
    return RaisedButton(
      onPressed: () async {
        if (_formKey.currentState.validate()) {
          _nombre = nombreController.text;
          _telefono = telefonoController.text;
          _direccion = direccionController.text;
          _email = emailController.text;
          _documento = documentoController.text;

          proveedor.nombres = _nombre;
          proveedor.telefono = _telefono;
          proveedor.direccion = _direccion;

          proveedor.email = _email;
          proveedor.documento = _documento;

          proveedor.tipodocumento = _tipoDocumento;
          proveedor.departamento = _departamento;
          proveedor.ciudad = _ciudad;

          if (proveedor.id == null) {
            proveedor.idUsuario = "1";
            proveedor.idSede = "1";
            proveedor.nit = "1";
            service.guardarProveedor(proveedor).then((value) {
              Services servicio = new Services();
              mostrarSnackbar('Se creo correctamente.').then((value) {
                final provider =
                    Provider.of<ManejadorEstado>(context, listen: false);
                provider.countPages = 0;
                servicio.listadoProveedores2(0, 'null').then((value) {
                  provider.listadoProveedores = value;

                  setState(() {});
                });
                Navigator.pop(context);
              });
            });
          } else {
            service.actualizarProveedor(proveedor).then((value) {
              Services servicio = new Services();
              mostrarSnackbar('Se actualizo correctamente').then((value) {
                final provider =
                    Provider.of<ManejadorEstado>(context, listen: false);
                provider.countPages = 0;
                servicio.listadoProveedores2(0, 'null').then((value) {
                  provider.listadoProveedores = value;
                  setState(() {});
                });
                Navigator.pop(context);
              });
            });
          }
        }
      },
      color: Colors.green,
      child: Text('Registrar', style: TextStyle(color: Colors.white)),
    );
  }
}
