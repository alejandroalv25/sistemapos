import 'package:flutter/material.dart';
 
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sistema POS',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Sistema POS'),
        ),
        body: Center(
          child: Container(
            child: Text('Dashboard'),
          ),
        ),
        drawer: Drawer(

        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text(
                'Sistema POS GEAD',
              ),
              decoration: BoxDecoration(
                color: Colors.green[300],
              ),
            ),
            ListTile(
              title: Text('Mis proveedores'),
              onTap: () {
                Navigator.pushNamed(context, 'proveedores');
              },
            ),
            ListTile(
              title: Text('Mis clientes'),
              onTap: () {
  
              },
            ),

            ListTile(
              title: Text('Mis productos'),
              onTap: () {

              },
            ),
            ListTile(
              title: Text('Mis sedes'),
              onTap: () {

              },
            ),

            ListTile(
              title: Text('Mi inventario'),
              onTap: () {

              },
            ),

            ListTile(
              title: Text('Mis ventas'),
              onTap: () {

              },
            ),
          ],
        ),
      ),
      ),
      theme: ThemeData(
          primarySwatch: Colors.green,
        ),
    );
  }
}