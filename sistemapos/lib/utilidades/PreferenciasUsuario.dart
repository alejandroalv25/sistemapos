import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario  {

  static final PreferenciasUsuario _instancias = new PreferenciasUsuario.internal();

  factory PreferenciasUsuario(){
    return _instancias;
  }

  PreferenciasUsuario.internal();

  SharedPreferences  _prefs;

  initPrefs() async{
    this._prefs = await SharedPreferences.getInstance();
  }

  get token{
    return _prefs.getString('token') ?? '';
  }

  set token(String valor){
    _prefs.setString('token', valor);
  }

  get correo{
    return _prefs.getString('correo') ?? '';
  }

  set correo(String valor){
    _prefs.setString('correo', valor);
  }

  get contrasena{
    return _prefs.getString('contrasena') ?? '';
  }

  set contrasena(String valor){
    _prefs.setString('contrasena', valor);
  }

}