class Ciudad {

  int id;
  String nombre;

  Ciudad({this.id, this.nombre});

  Ciudad.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        nombre = json['nombre'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nombre': nombre,
    };
  }
}