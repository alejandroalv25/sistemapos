class Departamento {

  int id;
  String nombre;

  Departamento({this.id, this.nombre});

  Departamento.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        nombre = json['nombre'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nombre': nombre,
    };
  }
}
