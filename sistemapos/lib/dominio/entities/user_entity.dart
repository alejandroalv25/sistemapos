class User {
  int id;
  String idUsuario;
  String idSede;
  String nit;
  String nombres;
  String telefono;
  String direccion;

  String email;
  String tipodocumento;
  String documento;
  String departamento;
  String ciudad;

  User({this.nombres, this.direccion, this.telefono});

  static List<User> getUsers() {
    return <User>[
      User(nombres: "Alejandro", direccion: "Calle 50", telefono: "8471926"),
      User(nombres: "Alejandro1", direccion: "Calle 50", telefono: "8471926"),
      User(nombres: "Alejandro2", direccion: "Calle 50", telefono: "8471926"),
      User(nombres: "Alejandro3", direccion: "Calle 50", telefono: "8471926"),
      User(nombres: "Alejandro4", direccion: "Calle 50", telefono: "8471926"),
    ];
  }

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        idUsuario = json['idUsuario'],
        idSede = json['idSede'],
        nit = json['nit'],
        nombres = json['nombres'],
        telefono = json['telefono'],
        direccion = json['direccion'],
        email = json['email'],
        tipodocumento = json['tipodocumento'],
        documento = json['documento'],
        departamento = json['departamento'],
        ciudad = json['ciudad'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'idUsuario': idUsuario,
      'idSede': idSede,
      'nit': nit,
      'nombres': nombres,
      'telefono': telefono,
      'direccion': direccion,
      'email': email,
      'tipodocumento': tipodocumento,
      'documento': documento,
      'departamento': departamento,
      'ciudad': ciudad
    };
  }
}
