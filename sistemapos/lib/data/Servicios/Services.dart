import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:sistemapos/dominio/entities/ciudad_entity.dart';
import 'package:sistemapos/dominio/entities/departamento_entity.dart';
import 'package:sistemapos/dominio/entities/user_entity.dart';

class Services {
  Future<List<User>> listadoProveedores() async {
    final url = "http://10.0.2.2/listadoProveedores";

    final res =
        await http.get(url, headers: {'Authorization': 'Bearer ' + '123'});

    Iterable l = json.decode(res.body);
    List<User> listaUsuarios =
        List<User>.from(l.map((model) => User.fromJson(model)));

    return listaUsuarios;
  }

  Future<List<User>> listadoProveedores2(int pagina, String filtro) async {
    if (filtro == "") {
      filtro = 'null';
    }

    final url = "http://10.0.2.2/listadoProveedores2/" +
        pagina.toString() +
        "/" +
        filtro;

    final res =
        await http.get(url, headers: {'Authorization': 'Bearer ' + '123'});

    Iterable l = json.decode(res.body);
    List<User> listaUsuarios =
        List<User>.from(l.map((model) => User.fromJson(model)));

    return listaUsuarios;
  }

  Future<bool> login(String usuario, String contrasena) async {
    final url = "http://10.0.2.2/login/" + usuario + "/" + contrasena;

    final res =
        await http.get(url, headers: {'Authorization': 'Bearer ' + '123'});
    Map<String, dynamic> restDecode = json.decode(res.body);

    if (restDecode['message'] == 'ok') {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> guardarProveedor(User proveedor) async {
    final url = "http://10.0.2.2/guardarProveedor";
    final res = await http.post(
      url,
      body: jsonEncode(proveedor.toJson()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    Map<String, dynamic> restDecode = json.decode(res.body);

    if (restDecode['message'] == 'ok') {
      print('Se guardo correctamente');
      return true;
    } else {
      print('No se guardo correctamente.');
      return false;
    }
  }

  Future<bool> actualizarProveedor(User proveedor) async {
    final url = "http://10.0.2.2/actualizarProveedor";
    final res = await http.post(
      url,
      body: jsonEncode(proveedor.toJson()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    Map<String, dynamic> restDecode = json.decode(res.body);

    if (restDecode['message'] == 'ok') {
      print('Se guardo correctamente');
      return true;
    } else {
      print('No se guardo correctamente.');
      return false;
    }
  }

  Future<bool> eliminarProveedor(int idProveedor) async {
    final url = "http://10.0.2.2/eliminarProveedor/" + idProveedor.toString();

    final res =
        await http.get(url, headers: {'Authorization': 'Bearer ' + '123'});
    Map<String, dynamic> restDecode = json.decode(res.body);

    if (restDecode['message'] == 'ok') {
      return true;
    } else {
      return false;
    }
  }

  Future<List<Departamento>> litadoDepartamento() async {
    final url = "http://10.0.2.2/lisadoDepartamento";

    final res =
        await http.get(url, headers: {'Authorization': 'Bearer ' + '123'});

    Iterable l = json.decode(res.body);
    List<Departamento> listaUsuarios =
        List<Departamento>.from(l.map((model) => Departamento.fromJson(model)));

    return listaUsuarios;
  }

  Future<List<Ciudad>> litadoCiudad(String idDepartamento) async {
    final url = "http://10.0.2.2/lisadoCiudad/" + idDepartamento.toString();

    final res =
        await http.get(url, headers: {'Authorization': 'Bearer ' + '123'});

    Iterable l = json.decode(res.body);
    List<Ciudad> listaUsuarios =
        List<Ciudad>.from(l.map((model) => Ciudad.fromJson(model)));

    return listaUsuarios;
  }
}
